# INSOMNIA
![](https://seeklogo.com/images/I/insomnia-logo-A35E09EB19-seeklogo.com.png)
## Introduction
insomnia est un outils de développement d'API permettant d'envoyer, de testé des requêtes sur des URL. Cela permet de testé une API sans avoir de front. Cela peut être utile dans diverses situation, par exemple:

 - premiers stade de développement (back en cours de création, front pas encore développer)
 - ajout de fonction (dans le back avant de l'intégré dans un front)
 - debug (lors d'un bug dans l'interface, permet de testé sans celui-ci pour savoir si le problème viens du back).

## fonctionalitées
Insomnia permet d'executer des requêtes sur des urls (comme un navigateur web) incluant:

 - dirigé une requête vers une URL
 - envoyer des requêtes de tout types (GET, POST, PUT, PATCH, DELETE, ...)
 - envoyer sur cette url des données de tous types (json, xml, ...)
 - import et export d'une collection ou de l'ensemble des collections  vers et à partir des formats Insomnia, Postman v2, HAR, OpenAPI, Swagger, WSDL, et cURL
    - application > Préférences > Data
 - **Configuration de variables d'environnement** pour réutiliser les valeurs dans plusieurs requêtes(URL, token, ...)
    - créer vos variables sous format json dans la modale disponible via: No Environnement (sommet de la barre latérale) > Manage Environnement
    - lorsque vous tapper le début d'une de vos clefs, l'auto-complétion vous propose de l'utilisé (la clef s'affiche sous forme d'étiquette et représente la valeur)
    - note: vous pouvez utilisé des variables pour construire des variables:
    ![](https://docs.insomnia.rest/assets/images/recursive-variables.png)
    - vous pouvez créer des sons répertoires clefs-valeur et leur assigné une couleur d'étiquette par exemple. le sous-répertoire est un "environnement de variables": par ex, deux environnement peuvent contenir la même clef avec une valeur différente, ce qui permet, lorsque l'on switch d'un environnement à l'autre, d'utiliser une même requête avec une url différente pour tester 2 serveurs sans avoir de quasi-doublons a tapper dans ces requêtes.
       - Note: attention, les environnements de variables sont propre à la collection dans laquel elles sont créer (on ne peut pas les utilisé depuis une autre collection)
       - Note: on peut créer des variables dans base environnement qui peuvent être réutilisé dans les sous-environnements.

### fonctionnalitées à creusé:
 - [proxi](https://docs.insomnia.rest/insomnia/http-proxy)




 ## sources
  - [documentation officiel get started](https://docs.insomnia.rest/insomnia/get-started) (en anglais)